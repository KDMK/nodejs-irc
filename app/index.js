const APP_PORT = 3000;

let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let people = {};

app.get('/', (req, res) => {
    res.send('<h1>Hello world</h1>');
});

app.get('/home', (req, res) => {
    res.sendFile(__dirname + '/home.html');
});

io.on('connection', (client) => {
    console.log(`${client.request.connection.remoteAddress} connected to chat.`);
    
    client.emit('chat message', 'Welcome to the awesome chat v0.01!');
    
    client.on('join', (name) => {
        people[client.id] = name;
        client.emit('update', 'You have connected to the server.');
        io.sockets.emit('update', `${name} has joined the channel.`);
        io.sockets.emit('update-people', people);
    });

    
    client.on('chat', (msg) => {
        console.log('message: ' + msg);
    });

    client.on('disconnect', () => {
        io.sockets.emit('update', people[client.id] + " has left the server.");
        delete people[client.id];
        io.sockets.emit("update-people", people);
    });

    client.on('send', (msg) => {
        io.emit('chat', people[client.id], msg);
    });
});

http.listen(APP_PORT, () => {
    console.log('Listening on port: ' + APP_PORT);
});